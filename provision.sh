#!/bin/sh

set -xe

export DEBIAN_FRONTEND=noninteractive

apt-get update
apt-get install --assume-yes \
    curl git build-essential \
    vim git htop tree lnav \
    ruby ruby-dev \
    libpq-dev postgresql \

gem install bundler
